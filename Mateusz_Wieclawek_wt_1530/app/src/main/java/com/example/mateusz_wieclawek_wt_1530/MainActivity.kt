package com.example.mateusz_wieclawek_wt_1530

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import java.lang.ArithmeticException
import java.lang.Exception
//import net.objecthunter.exp4j.ExpressionBuilder


class MainActivity : AppCompatActivity() {
    internal lateinit var clearButton: Button
    internal lateinit var result: Button

    internal lateinit var display: TextView
    var element:String=""
    internal lateinit var digits: Array<Button> //przyciski z liczbami
    internal lateinit var operations: Array<Button>  //przyciski z operacjami
    internal var numbers= mutableListOf<String>()//inicjalizacja pustej listy z aktualnie wybieranymi liczami
    internal var choosenOperations= mutableListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val buttonsIDs=arrayOf(R.id.button1,R.id.button2,R.id.button3,R.id.button4,R.id.button5,R.id.button6,R.id.button7,R.id.button8,R.id.button9,R.id.button0,R.id.buttonDot)//TODO: dodac wszystkie przyciski cyfry
        val buttonsIDsOper= arrayOf(R.id.buttonDiv,R.id.buttonPlus,R.id.buttonMinus,R.id.buttonMult)
        clearButton=findViewById(R.id.buttonCls)
        clearButton.setOnClickListener { view-> clearScrean() }
        result=findViewById(R.id.buttonResult)
        result.setOnClickListener{
                view->try
            {
            calculate()
            }
            catch (e:Exception)
            {
                display.text=e.message
            }
        }
        digits= (buttonsIDs.map{id->findViewById(id) as Button}).toTypedArray()
        operations=(buttonsIDsOper.map { id->findViewById(id) as Button }).toTypedArray()
        display=findViewById(R.id.result)
        operations.forEach { button->button.setOnClickListener{i->OperationButtonPresed(i as Button)} }

        digits.forEach { button->button.setOnClickListener{i->buttonPresed(i as Button)} }
    }

    private fun calculate() {
        if(element.isNotEmpty()) {
            numbers.add(element)

        }
        element=""
        var currentResult=0.0

       if((numbers.size-1)!=choosenOperations.size) {
            throw Exception("Bład")
       }
        for(operation in choosenOperations)
        {
            var a=numbers.get(0).toDouble()
            var b=numbers.get(1).toDouble()
            when(operation)
            {
                "+"->{currentResult=add(a,b)}
                "-"->{currentResult=minus(a,b)}
                "*" ->{currentResult=multiply(a,b)}
                "x"->{currentResult=multiply(a,b)}
                "/"->{currentResult=divvide(a,b)}
                else -> {
                    display.setText("Błąd")
                }
            }
            numbers.set(0,currentResult.toString())
            numbers.removeAt(1)
        }

        choosenOperations.clear()
        display.text=currentResult.toString()

    }

    @SuppressLint("SetTextI18n")
    private fun buttonPresed(button: Button) {

            element=element+button.text.toString()
            display.text=display.text.toString()+button.text.toString()
            print(button.text.toString())

    }
    private fun OperationButtonPresed(button: Button)
    {
        if(element.isNotEmpty()) {
            numbers.add(element)
        }
        element=""
        choosenOperations.add(button.getText().toString())
        display.text=display.text.toString()+button.text.toString()

    }

    private fun clearScrean() {
        display.text=""
        numbers.clear()
        choosenOperations.clear()

    }
    private fun add(a: Double,b:Double):Double{
        return a+b
    }
    private fun minus(a: Double,b:Double):Double{
        return a-b
    }
    private fun multiply(a: Double,b:Double):Double{
        return a*b
    }
    private fun divvide(a: Double,b:Double):Double {
        if(b==0.0) throw Exception("Nie można dzielić przez 0")
            return a / b

    }
}