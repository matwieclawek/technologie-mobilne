package com.example.mateusz_wieclawek_wt_1530_lab2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import org.json.JSONObject
import java.security.KeyStore

class HistoricRatesActivity : AppCompatActivity() {
    lateinit var todayRate: TextView
    lateinit var yesterdayRate: TextView
    lateinit var lineChart: LineChart
    lateinit var currencyCode: String
    private lateinit var data: Array<Pair<String,Double>>
    lateinit var button:Button
    lateinit var inputText: EditText
    lateinit var result: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_historic_rates)
        todayRate=findViewById(R.id.todayRate)
        yesterdayRate=findViewById(R.id.yesterdayRate)
        lineChart=findViewById(R.id.historicRatesChart)
        button=findViewById(R.id.button)
        inputText=findViewById(R.id.editText)
        result=findViewById(R.id.resultView)
        currencyCode=intent.getStringExtra("currencyCode")!!
        getHistoricRates()
        button.setOnClickListener { view->calculate() }
    }
    private fun getHistoricRates(){
        val queue=DataHolder.queue
        var url = "http://api.nbp.pl/api/exchangerates/rates/A/%s/last/30?format=json".format(currencyCode)

        if(DataHolder.table=="B") {
             url = "http://api.nbp.pl/api/exchangerates/rates/B/%s/last/30?format=json".format(currencyCode)

        }

        val historicRatesRequest= JsonObjectRequest( Request.Method.GET,url,null,
            Response.Listener {response->
                println("Historyczne dane Sukces!")
                loadHistoricData(response)
                showData()

            },Response.ErrorListener{
            println("Nie udało się")
        })
        queue.add(historicRatesRequest)
    }
    private fun showData(){
        todayRate.text=getString(R.string.textview,data.last().second)
        yesterdayRate.text=getString(R.string.yesterdayRate,data.last().second)

        val entries=ArrayList<Entry>()
        for((index,element)in data.withIndex()){
            entries.add(Entry(index.toFloat(),element.second.toFloat()))
        }
        val lineData=LineData(LineDataSet(entries,"Kursy"))

        lineChart.data=lineData
        val description=Description()
        description.text="Kurs %s z ostatnich 30 dni".format(currencyCode)
        lineChart.description=description
        lineChart.xAxis.valueFormatter=IndexAxisValueFormatter(data.map { it.first }.toTypedArray())
        lineChart.invalidate()

    }
    private fun loadHistoricData(response: JSONObject){
        response?.let {
            val rates=response.getJSONArray("rates")
            val ratesCount=rates.length()
            val tmpData= arrayOfNulls<Pair<String,Double>>(ratesCount)
            for(i in 0 until ratesCount)
            {
                val date=rates.getJSONObject(i).getString("effectiveDate")
                val rate=rates.getJSONObject(i).getDouble("mid")

                tmpData[i]=Pair(date,rate)
            }
            data=tmpData as Array<Pair<String,Double>>

        }
    }
    private fun calculate(){
        var a=0.0
        var b=0.0
        try {
            a = inputText.text.toString().toDouble()
        }
        catch (e: Exception)
        {
            e.message
        }
        a=(((a*100).toInt())/100).toDouble()
        b=a/data.last().second*100
        //b=((((a/data.last().second)*100).toInt()).toDouble())/100
        b=b.toDouble()
        b=b/100
        result.text=(a*data.last().second).toString()+"PLN"+" "+b+currencyCode
    }
}