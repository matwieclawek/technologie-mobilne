package com.example.mateusz_wieclawek_wt_1530_lab2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class activity_menu : AppCompatActivity() {
    internal lateinit var recylerViewButton:Button
    internal lateinit var goldButton:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        recylerViewButton=findViewById(R.id.recylerViewButton)
        goldButton=findViewById(R.id.golButton)

        recylerViewButton.setOnClickListener {
            val intent= Intent(this,MainActivity::class.java)
            startActivity(intent)
        }
        goldButton.setOnClickListener {
            val intent=Intent(this,GoldActivity::class.java)
            DataHolder.table="Z"
            startActivity(intent)
        }

    }
}