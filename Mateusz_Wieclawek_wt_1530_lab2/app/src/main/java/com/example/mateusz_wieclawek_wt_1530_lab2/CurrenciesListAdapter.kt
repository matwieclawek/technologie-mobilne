package com.example.mateusz_wieclawek_wt_1530_lab2

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CurrenciesListAdapter(var dataSet: Array<CurrencyDetails>, val context:Context):RecyclerView.Adapter<CurrenciesListAdapter.ViewHolder>() {
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val currencyCodeTextView: TextView
        val rateTextView: TextView
        val flagView: ImageView

        init {
            // Define click listener for the ViewHolder's View.
            currencyCodeTextView = view.findViewById(R.id.currencyView)
            rateTextView = view.findViewById(R.id.rateView)
            flagView = view.findViewById(R.id.imageView)

        }

    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.currency_row, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        val currency = dataSet[position]
        viewHolder.currencyCodeTextView.text = currency.currencyCode
        viewHolder.rateTextView.text = currency.rate.toString()
        viewHolder.flagView.setImageResource(currency.flag)
        viewHolder.itemView.setOnClickListener { goToDetails(currency.currencyCode) }

        // Return the size of your dataset (invoked by the layout manager)

    }
    private fun goToDetails(currencyCode: String)
    {
        val intent= Intent(context,HistoricRatesActivity::class.java).apply {
            putExtra("currencyCode", currencyCode)
        }
        context.startActivity(intent)
    }

    override fun getItemCount()=dataSet.size

}





