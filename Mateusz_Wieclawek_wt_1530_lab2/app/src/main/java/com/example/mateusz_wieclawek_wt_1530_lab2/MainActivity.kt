package com.example.mateusz_wieclawek_wt_1530_lab2
//Mateusz Więcławek wtorek 15:30
//Moja aplikacja obsługuje obie tabele, przelicznik został umieszczony w activity z historycznymi danymi,
//
//Nie udało mi się zrobić:
//*cen złota, sam nie wiem dlaczego występuje błąd podczas pobierania cen z api
//*strałki informującej o spadku lub wzroście ceny
//*jest tylko wykres pobierająćy dane z ostatnich 30 dni


import android.app.DownloadManager
import android.app.VoiceInteractor
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.Volley.newRequestQueue
import org.json.JSONArray
class MainActivity : AppCompatActivity() {
    internal lateinit var recycler: RecyclerView
    internal lateinit var adapter: CurrenciesListAdapter
    internal lateinit var next:Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DataHolder.prepare(applicationContext)
        next=findViewById(R.id.nextButton)
        recycler=findViewById(R.id.curreciesListRecyclerView)
        recycler.layoutManager=LinearLayoutManager(applicationContext)
        adapter=CurrenciesListAdapter(emptyArray(),this)
        recycler.adapter=adapter
        makeRequest("http://api.nbp.pl/api/exchangerates/tables/A?format=json")
        next.setOnClickListener{view->(changeCurrencies())}
      //  makeRequest("http://api.nbp.pl/api/exchangerates/tables/B?format=json")

    }
    fun changeCurrencies(){
        if(DataHolder.table=="A") {
            makeRequest("http://api.nbp.pl/api/exchangerates/tables/B?format=json")
            DataHolder.table="B"
        }
        else {
            makeRequest("http://api.nbp.pl/api/exchangerates/tables/A?format=json")
            DataHolder.table="A"
        }
    }
fun makeRequest(url:String){
    val queue=DataHolder.queue

    val curreciesRateRequest= JsonArrayRequest(Request.Method.GET,url,null,
    Response.Listener{
        response->println("Sukces")
        loadData(response)
        adapter.notifyDataSetChanged()
    },
    Response.ErrorListener{
        error->println("Error")
    })
    queue.add(curreciesRateRequest)

}
    private fun loadData(response:JSONArray?){
        response?.let {
            val rates=response.getJSONObject(0).getJSONArray("rates")
            val ratesCount=rates.length()
            val tmpData= arrayOfNulls<CurrencyDetails>(ratesCount)
            for(i in 0 until ratesCount)
            {
                val currencyCode=rates.getJSONObject(i).getString("code")
                val currencyRate=rates.getJSONObject(i).getDouble("mid")
                val currencyFlag=DataHolder.getFlag(currencyCode)
                val currencyObject=CurrencyDetails(currencyCode,currencyRate,currencyFlag)
                tmpData[i]=currencyObject
            }
            adapter.dataSet=tmpData as Array<CurrencyDetails>

        }
    }

}