package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.TextView
import android.widget.Button
import android.widget.Toast
import org.w3c.dom.Text


class MainActivity : AppCompatActivity() {
    internal lateinit var button: Button
    internal lateinit var scoreLabel: TextView
    internal lateinit var timeLabel: TextView

    private var score = 0
    private var started = false
    private lateinit var timer: CountDownTimer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button)
        scoreLabel = findViewById(R.id.scoreLabel)
        timeLabel = findViewById(R.id.timeLabel)

        initDisplay()
        timer= object : CountDownTimer(15000,1000){
            override fun onTick(millisUntilFinished: Long){
                setTime(millisUntilFinished/1000)
            }
            override fun onFinish(){
                    gameOver()

            }


        }
        button.setOnClickListener { view->
            buttonClicked()
        }

    }
    private fun setTime(l:Long) {
        val timeText = getString(R.string.timeLabel, l)
        timeLabel.text = timeText
    }


    @SuppressLint("StringFormatInvalid")
    private fun initDisplay() {
        setScore(0)

        setTime(0)

    }
    private fun setScore(score: Int){
        val scoreText = getString(R.string.scoreLabel, score)
        scoreLabel.text = scoreText


    }

    private fun buttonClicked() {
        if (started) {
            score += 1
           setScore(score)
        } else {
            startGame()
        }
    }

    private fun startGame(){
        started=true
        score=0
        button.text="Tap Me"
        timer.start()
    }

    private fun gameOver(){

        val message= getString(R.string.message, score)
        Toast.makeText(this@MainActivity,message,Toast.LENGTH_LONG).show()
        started=false
        score=0
        button.text="Cipka"
    }
}